\section{Introduction}
\label{sec:intro}

\IEEEPARstart{O}{ne} of the biggest problems Electricity Distribution Companies (EDCOs) face is that of electricity loss due to non-technical
sources. Those \textit{non-technical losses}, caused mainly by factors unrelated to the electricity transmission process 
(like frauds or broken power meters), represented about 13\% of all electricity distributed in 2012, according to a regional Brazilian distribution company~\cite{ANEEL2012}.

All that loss imposes a great profit reduction to the EDCOs, which motivates them to recover it by 
increasing the price of the distributed electricity. However, in order to prevent abuses and encourage EDCOs to improve 
their services, the brazilian regulatory agency of electricity (ANEEL) sets a threshold on the amount of non-technical loss
that can be covered by increasing charges, which is known as \textit{non-technical loss reducing goal}.

In practice, the existence of this goal implies that EDCOs cannot charge clients more than a fixed 
value per kilowatt-hour to attenuate those non-technical losses.
The non-technical loss reducing goal (and indirectly the maximum kilowatt-hour charge) is established by ANEEL by studying the EDCO's 
profile and the historical non-technical losses behaviour at similar regions of the country.
Additionally, the EDCOs are required to \textit{reduce} the kilowatt-hour electricity charge if they manage to mitigate non-technical
electricity loss above the established goal. 
This means that there is no gain to the EDCOs if they reduce the non-technical losses above the established goal.

Usually, the non-technical loss reducing goal is given for a period, which is usually three years,
defining a \textit{non-technical loss reducing curve}.
If EDCOs don't meet the imposed non-technical loss reduction goal in a given year, their profit in that year
is reduced by the corresponding value of the electricity under the goal. For example, suppose an EDCO with a historical
non-technical electricity loss level of 15\%. That means that considering only the non-technical losses, in order to sell 85 GW 
of electricity the EDCO must buy 100 GW from the electricity generation companies. If the regulatory agency establishes a hipotetical 
goal of 10\%, the EDCO is authorised to make an increase in the electricity charge to cover 10 of the 15 GW lost, and must mitigate 
the other 5 GW, or assume the corresponding profit loss.

In order to fight non-technical electricity losses and reach the goal, the EDCOs define \textit{Non-Technical Losses Reduction Plans} 
for a given period. Those plans consist of the non-technical loss reduction curve, a portfolio of \textit{Non-technical Loss Reducing Actions}, 
\textit{yearly budgets} for different \textit{resources}, which will be spent on the execution of the actions and 
an \textit{internal return rate}. A great variety of actions compose the portfolio, and each one of them has several distinct 
characteristics which must be considering during planning. One of them is the action's \textit{market}, representing how many times 
the action can be executed during the whole plan. Another one is the \textit{yearly market}, which is analogous to the market, but represents the amount of
times an action can be executed during each year of the plan. Other important aspects are the action's \textit{cost}, \textit{recovered electricity}, \textit{electricity value} and 
\textit{dependency}. Firstly, each action consumes different portions of each budget, that is called the action's cost. Also, even though an action 
consumes the budgets only on the year it was executed, the electricity recovered by the action may be spread over the following years. Another important 
characteristic is that different actions may have a diferent values for each unit of electricity recovered, so in order to calculate the action's profit, 
the \textit{electricity value} must be defined for each action. Finally, some actions may require other actions to be taken before them, so a 
\textit{dependency} relation between those actions must be defined.

This scenario introduces an interesting problem to the EDCOs: given an investment portfolio comprised of 
several non-technical loss reducing actions, a non-technical loss reduction curve and yearly budgets, what is the best possible 
allocation of actions that maximizes the return of the investment, that is, the 
allocation which yields the greatest profit for the EDCO?
If the EDCO chooses to do nothing, it will not be able to charge for some portion of the
distributed electricity, and since this usually costs more 
than performing the non-technical loss reducing actions, doing nothing is not
the optimal course of action. On the other hand, reducing the loss to a greater extent than the
imposed reduction curve is also not the optimal decision, since the over-reduced electricity loss does not
increase the overall EDCO profit.

The current methodology used by EDCOs is to manually select the loss reducing actions in
a heuristic trial-and-error fashion. As we shall present in this work, choosing the best non-technical loss reducing actions is
a complex combinatorial problem, thus the current \textit{de facto} procedure hardly finds an optimal solution of the problem.
Since reducing non-technical losses is an important activity of EDCOs, this motivates the use of computational techniques to 
find the best, or at least good solutions to the problem.

In this paper, we formulate the problem previously presented formally as a generalization of the knapsack problem, a well known problem in 
Combinatorial Optimization. Due to the novelty of the formulation and some constraints on the availability of real instances of the problem, 
an instance generator is also presented, and we define a set of benchmark instances. Two heuristics are also proposed to solve the problem, one based on a greedy approach, 
named GALP, and the other on the Tabu Search metaheurisic, the TSLP. Experimental tests are then conducted, solving the instances with the CPLEX solver and the two proposed
heuristics, and a comparison between those three approaches is made.

The remainder of the paper is organized as follows: Section~\ref{sec:model} defines formally the
problem of choosing non-technical loss reducing actions, and explain its relation to the knapsack problem.
Section~\ref{sec:approach} introduces the approches used for solving the problem.
Section~\ref{sec:exp_data} introduces the instance generator used to create our set of benchmark instances. 
In Section~\ref{sec:exp_results} we present our experimental evaluation. Finally, in Section~\ref{sec:conclu} 
we make our concluding remarks.
