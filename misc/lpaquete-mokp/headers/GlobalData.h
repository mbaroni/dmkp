/*
 * GlobalData.h
 *
 *  Created on: 2010/01/16
 *      Author: disbeat
 */

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>


using namespace std;

class GlobalData {

public:
	static vector<string> tokenize(string s);
	static void setSOLUTION_SIZE();

	static int MAX_WEIGHT;
	static int NUM_POINTS;
	static int SOLUTION_SIZE; //number of bytes dividing by the size of long

	static unsigned int *POWVALUES;

	static int COUNTER;

	static double R;
	static int ALFA;
};
